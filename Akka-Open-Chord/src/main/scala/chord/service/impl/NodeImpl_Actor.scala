/***************************************************************************
 *                                                                         *
 *                           NodeImpl_Actor.scala                          *
 *                            -------------------                          *
 *   date                 : 8.12.2015                                      *
 *   email                : heath.french@utah.edu                          *
 *                                                                         *
 *                                                                         *
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   A copy of the license can be found in the license.txt file supplied   *
 *   with this software or at: http://www.gnu.org/copyleft/gpl.html        *
 *                                                                         *
 ***************************************************************************/

package main.scala.chord.service.impl

import main.java.util.logging.Logger.LogLevel.DEBUG;
import main.java.util.logging.Logger.LogLevel.INFO;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.TimeUnit

import main.java.chord.com.CommunicationException;
import main.java.chord.com.Endpoint;
import main.java.chord.com.Entry;
import main.java.chord.com.Node;
import main.java.chord.com.RefsAndEntries;
import main.java.chord.data.ID;
import main.java.chord.data.URL;
import main.java.chord.service.impl.HashFunction;
import main.java.util.logging.Logger;


import akka.actor._
import akka.remote._
import akka.pattern._
import akka.util.Timeout

import com.typesafe.config.ConfigFactory

import scala.concurrent._
import scala.concurrent.duration.FiniteDuration
import scala.collection.JavaConversions._

class NodeImpl_Actor(nodeSystem1 : ActorSystem, nodeURL1 : URL) extends Node{
  
  private val nodeSystem : ActorSystem = nodeSystem1
  this.nodeURL = nodeURL1
  this.nodeID = HashFunction.getHashFunction.createUniqueNodeID(this.nodeURL)

  override def findSuccessor(key: ID): Node = {
    implicit val timeout = Timeout(10, TimeUnit.SECONDS)
    val actor : ActorRef = this.getConnection()
    
    if(actor != null){
      try{ // tries to retrieve result from other actor
        val future : Future[Any] = actor.ask(NodeConditions.findSuccessor(key))
        val ref = Await.result(future, new FiniteDuration(10, TimeUnit.SECONDS))
        ref match{
          case exception : Throwable => throw exception
          case result : URL => {
            val node : Node = new NodeImpl_Actor(nodeSystem, result)
            return node
          }
        }
      }
      catch{ // if an error occurs, than throws error
        case e : Exception => throw new CommunicationException(e.toString())
      }
    }
    else{
      throw new CommunicationException()
    }
  }

  override def notify(potentialPredecessor: Node): List[Node] = {
    implicit val timeout = Timeout(10, TimeUnit.SECONDS)
    val actor : ActorRef = this.getConnection()
    
    if(actor != null){
      try{ // tries to retrieve result from other actor
        val future : Future[Any] = actor.ask(NodeConditions.actualNotify(potentialPredecessor.getNodeURL))
        val ref = Await.result(future, new FiniteDuration(10, TimeUnit.SECONDS))
        ref match{
          case exception : Throwable => throw exception
          case result : List[URL] => {
            var newList : List[Node] = new java.util.ArrayList[Node]()
            for(current : URL <- result){
              newList.add(new NodeImpl_Actor(nodeSystem, current))
            }
            return newList
          }
        }
      }
      catch{ // if an error occurs, than throws error
        case e : Exception => throw new CommunicationException(e.toString())
      }
    }
    else{
      throw new CommunicationException()
    }
  }

  override def notifyAndCopyEntries(potentialPredecessor: Node): RefsAndEntries = {
    implicit val timeout = Timeout(10, TimeUnit.SECONDS)
    val actor : ActorRef = this.getConnection()
    
    if(actor != null){
      try{ // tries to retrieve result from other actor
        val future : Future[Any] = actor.ask(NodeConditions.notifyAndCopyEntries(potentialPredecessor.getNodeURL))
        val ref = Await.result(future, new FiniteDuration(10, TimeUnit.SECONDS))
        ref match{
          case exception : Throwable => throw exception
          case result : UrlsAndEntries => {
            var nodes : LinkedList[Node] = new LinkedList[Node]()
            for(current : URL <- result.urls){
              nodes.add(new NodeImpl_Actor(nodeSystem, current))
            }
            return new RefsAndEntries(nodes, result.entries)
          }
        }
      }
      catch{ // if an error occurs, than throws error
        case e : Exception => throw new CommunicationException(e.toString())
      }
    }
    else{
      throw new CommunicationException()
    }
  }

  override def ping(): Unit = {
    implicit val timeout = Timeout(10, TimeUnit.SECONDS)
    val actor : ActorRef = this.getConnection()
    
    if(actor != null){
      return
    }
    else{
      throw new CommunicationException()
    }
  }

  override def insertEntry(entryToInsert: Entry): Unit = {
    implicit val timeout = Timeout(10, TimeUnit.SECONDS)
    val actor : ActorRef = this.getConnection()
    
    if(actor != null){
      try{
        val future : Future[Any] = actor.ask(NodeConditions.insertEntry(entryToInsert))
        val ref = Await.result(future, new FiniteDuration(10, TimeUnit.SECONDS))
        ref match{
          case exception : Throwable => throw exception
          case _ => return
        }
      }
      catch{ // if an error occurs, than throws error
        case e : Exception => throw e
      }
    }
    else{
      throw new CommunicationException()
    }
  }

  override def insertReplicas(entries: Set[Entry]): Unit = {
    implicit val timeout = Timeout(10, TimeUnit.SECONDS)
    val actor : ActorRef = this.getConnection()
    
    if(actor != null){
      try{
        val future : Future[Any] = actor.ask(NodeConditions.insertReplicas(entries))
        val ref = Await.result(future, new FiniteDuration(10, TimeUnit.SECONDS))
        ref match{
          case exception : Throwable => throw exception
          case _ => return
        }
      }
      catch{ // if an error occurs, than throws error
        case e : Exception => throw new CommunicationException(e.toString())
      }
    }
    else{
      throw new CommunicationException()
    }
  }

  override def removeEntry(entryToRemove: Entry): Unit = {
    implicit val timeout = Timeout(10, TimeUnit.SECONDS)
    val actor : ActorRef = this.getConnection()
    
    if(actor != null){
      try{
        val future : Future[Any] = actor.ask(NodeConditions.removeEntry(entryToRemove))
        val ref = Await.result(future, new FiniteDuration(10, TimeUnit.SECONDS))
        ref match{
          case exception : Throwable => throw exception
          case _ => return
        }
      }
      catch{ // if an error occurs, than throws error
        case e : Exception => throw new CommunicationException(e.toString())
      }
    }
    else{
      throw new CommunicationException()
    }
  }

  override def removeReplicas(sendingNode: ID, replicasToRemove: Set[Entry]): Unit = {
    implicit val timeout = Timeout(10, TimeUnit.SECONDS)
    val actor : ActorRef = this.getConnection()
    
    if(actor != null){
      try{
        val future : Future[Any] = actor.ask(NodeConditions.removeReplicas(sendingNode, replicasToRemove))
        val ref = Await.result(future, new FiniteDuration(10, TimeUnit.SECONDS))
        ref match{
          case exception : Throwable => throw exception
          case _ => return
        }
      }
      catch{ // if an error occurs, than throws error
        case e : Exception => throw new CommunicationException(e.toString())
      }
    }
    else{
      throw new CommunicationException()
    }
  }

  override def retrieveEntries(id: ID): Set[Entry] = {
    implicit val timeout = Timeout(10, TimeUnit.SECONDS)
    val actor : ActorRef = this.getConnection()
    
    if(actor != null){
      try{ // tries to retrieve result from other actor
        val future : Future[Any] = actor.ask(NodeConditions.retrieveEntries(id))
        val ref = Await.result(future, new FiniteDuration(10, TimeUnit.SECONDS))
        ref match{
          case exception : Throwable => throw exception
          case result : Set[Entry] => return result
        }
      }
      catch{ // if an error occurs, than throws error
        case e : Exception => throw new CommunicationException(e.toString())
      }
    }
    else{
      throw new CommunicationException()
    }
  }

  override def leavesNetwork(predecessor: Node): Unit = {
    implicit val timeout = Timeout(10, TimeUnit.SECONDS)
    val actor : ActorRef = this.getConnection()
    
    if(actor != null){
      try{
        val future : Future[Any] = actor.ask(NodeConditions.leavesNetwork(predecessor.getNodeURL))
        val ref = Await.result(future, new FiniteDuration(10, TimeUnit.SECONDS))
        ref match{
          case exception : Throwable => throw exception
          case _ => return
        }
      }
      catch{ // if an error occurs, than throws error
        case e : Exception => throw new CommunicationException(e.toString())
      }
    }
    else{
      throw new CommunicationException()
    }
  }

  override def disconnect(): Unit = {
    implicit val timeout = Timeout(10, TimeUnit.SECONDS)
    val actor : ActorRef = this.getConnection()
    
    if(actor != null){
      actor ! NodeConditions.disconnect()
      return
    }
  }
  
  def getConnection() : ActorRef = {
    val address : String = "akka.tcp://ChordSystem" + nodeURL.getPort + "@" + nodeURL.getHost + ":" + nodeURL.getPort + "/user/nodeImpl"
    val selection : ActorSelection = nodeSystem.actorSelection(address)
    
    try{ // tries to get the reference to an actor at the current URL
    val future : Future[ActorRef] = selection.resolveOne(new FiniteDuration(10, TimeUnit.SECONDS)).mapTo[ActorRef]
    val ref : ActorRef = Await.result(future, new FiniteDuration(10, TimeUnit.SECONDS))
    return ref
    }
    catch{ // if an error occurs, than returns a null
      case e : Any => return null
    }
  }

}