/***************************************************************************
 *                                                                         *
 *                           UrlsAndEntries.scala                          *
 *                            -------------------                          *
 *   date                 : 8.12.2015                                      *
 *   email                : heath.french@utah.edu                          *
 *                                                                         *
 *                                                                         *
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   A copy of the license can be found in the license.txt file supplied   *
 *   with this software or at: http://www.gnu.org/copyleft/gpl.html        *
 *                                                                         *
 ***************************************************************************/

package main.scala.chord.service.impl

import main.java.util.logging.Logger.LogLevel.DEBUG;
import main.java.util.logging.Logger.LogLevel.INFO;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.LinkedList;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import main.java.chord.com.CommunicationException;
import main.java.chord.com.Entry;
import main.java.chord.com.Node;
import main.java.chord.com.Proxy;
import main.java.chord.com.RefsAndEntries;
import main.java.chord.data.ID;
import main.java.chord.data.URL;
import main.java.chord.service.AsynChord;
import main.java.chord.service.Chord;
import main.java.chord.service.ChordCallback;
import main.java.chord.service.ChordFuture;
import main.java.chord.service.ChordRetrievalFuture;
import main.java.chord.service.Key;
import main.java.chord.service.Report;
import main.java.chord.service.ServiceException;
import main.java.util.logging.Logger;

import akka.actor._
import akka.remote._
import com.typesafe.config.ConfigFactory

import scala.collection.JavaConversions._

case class UrlsAndEntries(refs : List[URL], newEntries : Set[Entry]) {
  val urls : List[URL] = refs
  val entries : Set[Entry] = newEntries
  
  /*def getRefsAndEntries(system : ActorSystem) : RefsAndEntries = {
    var nodes : LinkedList[Node] = new LinkedList[Node]()
    for(current : URL <- urls){
      nodes.add(new NodeImpl_Actor(system, current))
    }
    
    return new RefsAndEntries(nodes, entries)
  }*/
  
  //def getUrls() : List[URL] = urls
  
  //def getEntries() : Set[Entry] = entries
  
}