/***************************************************************************
 *                                                                         *
 *                           ChordImpl_Actor.scala                         *
 *                            -------------------                          *
 *   date                 : 8.12.2015                                      *
 *   email                : heath.french@utah.edu                          *
 *                                                                         *
 *                                                                         *
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   A copy of the license can be found in the license.txt file supplied   *
 *   with this software or at: http://www.gnu.org/copyleft/gpl.html        *
 *                                                                         *
 ***************************************************************************/

package main.scala.chord.service.impl

import main.java.util.logging.Logger.LogLevel.DEBUG
import main.java.util.logging.Logger.LogLevel.INFO

import java.io.Serializable
import java.util.HashSet
import java.util.List
import java.util.Set
import java.util.LinkedList
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledThreadPoolExecutor
import java.util.concurrent.TimeUnit

import main.java.chord.com.CommunicationException
import main.java.chord.com.Entry
import main.java.chord.com.Node
import main.java.chord.com.RefsAndEntries
import main.java.chord.data.ID
import main.java.chord.data.URL
import main.java.chord.service.AsynChord
import main.java.chord.service.Chord
import main.java.chord.service.ChordCallback
import main.java.chord.service.ChordFuture
import main.java.chord.service.ChordRetrievalFuture
import main.java.chord.service.Key
import main.java.chord.service.Report
import main.java.chord.service.ServiceException
import main.java.util.logging.Logger
import akka.actor._
import akka.remote._

import com.typesafe.config.ConfigFactory

import scala.collection.JavaConversions._
import main.java.chord.service.ServiceException
import main.java.chord.service._
import main.java.chord.service.impl.CheckPredecessorTask;
import main.java.chord.service.impl.ChordInsertFuture;
import main.java.chord.service.impl.ChordRemoveFuture;
import main.java.chord.service.impl.ChordRetrievalFutureImpl;
import main.java.chord.service.impl.Entries;
import main.java.chord.service.impl.FixFingerTask;
import main.java.chord.service.impl.HashFunction;
import main.java.chord.service.impl.References;
import main.java.chord.service.impl.StabilizeTask;

class NodeActor(newSystem : ActorSystem, newChord : ChordImpl_Actor, newReferences : References, newEntries : Entries, newNodeID : ID, newNodeURL : URL) extends Actor {
  val chord : ChordImpl_Actor = newChord
  val nodeSystem : ActorSystem = newSystem
  val references : References = newReferences
  val entries : Entries = newEntries
  val nodeID : ID = newNodeID
  val nodeURL : URL = newNodeURL
  val asyncExecutor : Executor = chord.getAsyncExecutor()
  
  def notify(potentialPredecessor : Node) : LinkedList[Node] = {
    val result = new LinkedList[Node]()
    if (this.references.getPredecessor != null) {
      result.add(this.references.getPredecessor)
    } else {
      result.add(potentialPredecessor)
    }
    result.addAll(this.references.getSuccessors)
    this.references.addReferenceAsPredecessor(potentialPredecessor)
    return result
  }
        
  override def receive = 
  {
    case NodeConditions.actualNotify(potentialPredecessor : URL) => {
      val node : Node = new NodeImpl_Actor(nodeSystem, potentialPredecessor)
      var result = new LinkedList[Node]()
      try {
        result = this.notify(node)
        var urlVersion = new java.util.LinkedList[URL]()
        for(current : Node <- result){
          urlVersion.add(current.getNodeURL)
        }
        sender ! urlVersion
      }
      catch{
        case e : Throwable => {
          sender ! e
        }
      }
    }
    
    // won't be used.
    case NodeConditions.disconnect() => {}
    
    case NodeConditions.findSuccessor(key : ID) => {
      try {
        val x : String = sender.toString()
        val result : Node = chord.findSuccessor(key)
        sender ! result.getNodeURL
      }
      catch{
        case e : Throwable => {
          sender ! e
        }
      }
    }
    
    case NodeConditions.insertEntry(entryToInsert : Entry) => {
      try{
        if ((this.references.getPredecessor != null) && 
          !entryToInsert.getId.isInInterval(this.references.getPredecessor.getNodeID, this.nodeID)) {
          this.references.getPredecessor.insertEntry(entryToInsert)
          sender ! 0
        }
        else {
          this.entries.add(entryToInsert)
          val newEntries = new HashSet[Entry]()
          newEntries.add(entryToInsert)
          val mustBeFinal = new HashSet[Entry](newEntries)
          for (successor : Node <- references.getSuccessors()) {
            this.asyncExecutor.execute(new Runnable() {
      
              def run() {
                try {
                  successor.insertReplicas(mustBeFinal)
                } catch {
                  case e: CommunicationException => 
                }
              }
            })
          }
         sender ! 0
        }
      }
      catch{
        case e : Throwable => {
          sender ! e
        }
      }
    }
    
    case NodeConditions.insertReplicas(entries : Set[Entry]) => {
      try{
        sender ! this.entries.addAll(entries)
      }
      catch{
        case e : Throwable => {
          sender ! e
        }
      }
    }
    
    case NodeConditions.leavesNetwork(predecessor : URL) => {
      try{
        sender ! this.references.removeReference(this.references.getPredecessor())
      }
      catch{
        case e : Throwable => {
          sender ! e
        }
      }
    }
    
    case NodeConditions.notifyAndCopyEntries(potentialPredecessor : URL) => {
      try {
        val node : Node = new NodeImpl_Actor(nodeSystem, potentialPredecessor)
        val copiedEntries = this.entries.getEntriesInInterval(this.nodeID, node.getNodeID)
        val result : List[Node] = this.notify(node)
        var urlVersion = new java.util.LinkedList[URL]()
        for(current : Node <- result){
          urlVersion.add(current.getNodeURL)
        }
        val uae : UrlsAndEntries = UrlsAndEntries(urlVersion, copiedEntries)
        sender ! uae
      }
      catch{
        case e : Throwable => {
          sender ! e
        }
      }
    }
    
    case NodeConditions.ping() => {
      sender ! 0
    }
    
    case NodeConditions.removeEntry(entryToRemove : Entry) => {
      try {
        if (this.references.getPredecessor != null && 
          !entryToRemove.getId.isInInterval(this.references.getPredecessor.getNodeID, this.nodeID)) {
            this.references.getPredecessor.removeEntry(entryToRemove)
            sender ! 0
        }
        else {
          this.entries.remove(entryToRemove)
          val entriesToRemove = new HashSet[Entry]()
          entriesToRemove.add(entryToRemove)
          val successors = this.references.getSuccessors
          val id = this.nodeID
          for (successor <- successors) {
            this.asyncExecutor.execute(new Runnable() {
      
              def run() {
                try {
                  successor.removeReplicas(id, entriesToRemove)
                } catch {
                  case e: CommunicationException => 
                }
              }
            })
          }
          sender ! 0
        }
      }
      catch{
        case e : Throwable => {
          sender ! e
        }
      }
    }
    
    case NodeConditions.removeReplicas(sendingNodeID : ID, replicasToRemove : Set[Entry]) => {
      try{
        if (replicasToRemove.size == 0) {
          val allReplicasToRemove = this.entries.getEntriesInInterval(this.nodeID, sendingNodeID)
          this.entries.removeAll(allReplicasToRemove)
          } 
        else {
          this.entries.removeAll(replicasToRemove)
        }
        sender ! 0
      }
      catch{
        case e : Throwable => {
          sender ! e
        }
      }
    }
    
    case NodeConditions.retrieveEntries(id : ID) => {
      try{
        if (this.references.getPredecessor != null && 
          !id.isInInterval(this.references.getPredecessor.getNodeID, this.nodeID)) {
          sender ! this.references.getPredecessor.retrieveEntries(id)
        }
        else{
          sender ! this.entries.getEntries(id)
        }
      }
      catch{
        case e : Throwable => {
          sender ! e
        }
      }
    }
    
    case _ => sender ! new Throwable("Recieved Unexpected Message")
  }
}

class ChordImpl_Actor() extends Chord with AsynChord with Report{
  
  private val ASYNC_CALL_THREADS : Int = 5 //java.lang.Integer.parseInt(System.getProperty(classOf[ChordImpl_Actor].getName + ".AsyncThread.no"))

  private val STABILIZE_TASK_START : Long = java.lang.Integer.parseInt(System.getProperty("main.java.chord.service.impl.ChordImpl.StabilizeTask.start"))

  private val STABILIZE_TASK_INTERVAL : Long = java.lang.Integer.parseInt(System.getProperty("main.java.chord.service.impl.ChordImpl.StabilizeTask.interval"))

  private val FIX_FINGER_TASK_START : Long = java.lang.Integer.parseInt(System.getProperty("main.java.chord.service.impl.ChordImpl.FixFingerTask.start"))

  private val FIX_FINGER_TASK_INTERVAL : Long = java.lang.Integer.parseInt(System.getProperty("main.java.chord.service.impl.ChordImpl.FixFingerTask.interval"))

  private val CHECK_PREDECESSOR_TASK_START : Long = java.lang.Integer.parseInt(System.getProperty("main.java.chord.service.impl.ChordImpl.CheckPredecessorTask.start"))

  private val CHECK_PREDECESSOR_TASK_INTERVAL : Long = java.lang.Integer.parseInt(System.getProperty("main.java.chord.service.impl.ChordImpl.CheckPredecessorTask.interval"))

  private val NUMBER_OF_SUCCESSORS = if ((java.lang.Integer.parseInt(System.getProperty("main.java.chord.service.impl.ChordImpl.successors")) < 
    1)) 1 else java.lang.Integer.parseInt(System.getProperty("main.java.chord.service.impl.ChordImpl.successors"))

  protected var logger : Logger = Logger.getLogger(this.getClass.getName() + ".unidentified")
  this.logger.debug("Logger initialized.");
  private var localNode : NodeImpl_Actor = _
  private var entries : Entries = _
  private var maintenanceTasks : ScheduledExecutorService = new ScheduledThreadPoolExecutor(3, new ChordThreadFactory("MaintenanceTaskExecution"))
  private var asyncExecutor : ExecutorService = Executors.newFixedThreadPool(ASYNC_CALL_THREADS, new ChordThreadFactory("AsynchronousExecution"))
  private var actorSystem : ActorSystem = _
  
  /**
   * ThreadFactory used with Executor services.
   * 
   * @author sven
   * 
   */
  private class ChordThreadFactory(private var executorName : String) extends java.util.concurrent.ThreadFactory {

    def newThread(r : Runnable) : Thread = {
      val newThread : Thread = new Thread(r);
      newThread.setName(this.executorName + "-" + newThread.getName());
      return newThread;
    }

  }
  
  protected var references : References = _
  private var hashFunction : HashFunction = HashFunction.getHashFunction();
  private var localURL : URL = _
  private var localID : ID = _
  
  this.logger = Logger.getLogger(this.getClass.getName() + ".unidentified")
  this.logger.debug("Logger initialized.");
  logger.info("ChordImpl initialized!");
  
  final def getAsyncExecutor() : Executor = {
    if (this.asyncExecutor == null) {
      throw new NullPointerException("ChordImpl.asyncExecutor is null!")
    }
    return this.asyncExecutor
  }
  
  final def findSuccessor(key : ID) : Node = {
     if (key == null) {
      val e = new NullPointerException("ID to find successor for may not be null!")
      this.logger.error("Null pointer.", e)
      throw e
    }
    val debug = this.logger.isEnabledFor(DEBUG)
    val successor = this.references.getSuccessor
    if (successor == null) {
      if (this.logger.isEnabledFor(INFO)) {
        this.logger.info("I appear to be the only node in the network, so I am " + 
          "my own " + 
          "successor; return reference on me: " + 
          this.getID)
      }
     return this.localNode
    } else if (key.isInInterval(this.getID, successor.getNodeID) || key == successor.getNodeID) {
      if (debug) {
        this.logger.debug("The requested key lies between my own and my " + 
          "successor's node id; therefore return my successor.")
      }
      try {
        if (debug) {
          this.logger.debug("Returning my successor " + successor.getNodeID + " of type " + 
            successor.getClass)
        }
        return successor
      } catch {
        case e: Exception => {
          this.logger.warn("Successor did not respond! Removing it from all " + "lists and retrying...")
          this.references.removeReference(successor)
          return findSuccessor(key)
        }
      }
    } else {
      val closestPrecedingNode = this.references.getClosestPrecedingNode(key)
      try {
        if (debug) {
          this.logger.debug("Asking closest preceding node known to this node for closest preceding node " + 
            closestPrecedingNode.getNodeID + 
            " concerning key " + 
            key + 
            " to look up")
        }
        return closestPrecedingNode.findSuccessor(key)
      } catch {
        case e: CommunicationException => {
          this.logger.error("Communication failure while requesting successor " + 
            "for key " + 
            key + 
            " from node " + 
            closestPrecedingNode.toString + 
            " - looking up successor for failed node " + 
            closestPrecedingNode.toString)
          this.references.removeReference(closestPrecedingNode)
          return findSuccessor(key)
        }
      }
    }
  }
  
  override def getURL() : URL = this.localURL

  @throws(classOf[IllegalStateException])
  override def setURL(nodeURL : URL) = {
    if (nodeURL == null) {
      val e = new NullPointerException("Cannot set URL to null!")
      this.logger.error("Null pointer", e)
      throw e
    }
    if (this.localNode != null) {
      val e = new IllegalStateException("URL cannot be set after creating or joining a network!")
      this.logger.error("Illegal state.", e)
      throw e
    }
    this.localURL = nodeURL
    this.logger.info("URL was set to " + nodeURL)
  }

  override def getID() : ID = this.localID

  @throws(classOf[IllegalStateException])
  override def setID(nodeID : ID) = {
    if (nodeID == null) {
      val e = new NullPointerException("Cannot set ID to null!")
      this.logger.error("Null pointer", e)
      throw e
    }
    if (this.localNode != null) {
      val e = new IllegalStateException("ID cannot be set after creating or joining a network!")
      this.logger.error("Illegal state.", e)
      throw e
    }
    this.localID = nodeID
    this.logger = Logger.getLogger(classOf[ChordImpl_Actor].getName + "." + this.localID)
  }

  @throws(classOf[ServiceException])
  override def create() = {
    if (this.localNode != null) {
      throw new ServiceException("Cannot create network; node is already connected!")
    }
    if (this.localURL == null) {
      throw new ServiceException("Node URL is not set yet!")
    }
    if (this.getID == null) {
      this.setID(this.hashFunction.createUniqueNodeID(this.localURL))
    }
    this.createHelp()
  }

  @throws(classOf[ServiceException])
  override def create(localURL1 : URL) = {
    if (localURL1 == null) {
      throw new NullPointerException("At least one parameter is null which is not permitted!")
    }
    if (this.localNode != null) {
      throw new ServiceException("Cannot create network; node is already connected!")
    }
    this.localURL = localURL1
    if (this.getID == null) {
      this.setID(this.hashFunction.createUniqueNodeID(this.localURL))
    }
    this.createHelp()
  }

  @throws(classOf[ServiceException])
  override def create(localURL1 : URL, localID1 : ID) = {
    if (localURL1 == null || localID1 == null) {
      throw new IllegalArgumentException("At least one parameter is null which is not permitted!")
    }
    if (this.localNode != null) {
      throw new ServiceException("Cannot create network; node is already connected!")
    }
    this.localURL = localURL1
    this.setID(localID1)
    this.createHelp()
  }
  
  private def createHelp(){
    this.logger.debug("Help method for creating a new Chord ring invoked.")
    this.entries = new Entries()
    if (NUMBER_OF_SUCCESSORS >= 1) {
      this.references = new References(this.getID, this.getURL, NUMBER_OF_SUCCESSORS, this.entries)
    } else {
      throw new RuntimeException("NUMBER_OF_SUCCESSORS intialized with wrong value! " + 
        NUMBER_OF_SUCCESSORS)
    }
    //  + ", akka.remote.netty.tcp.hostname=\"" + localURL.getHost + "\""
    val config = ConfigFactory.parseString("akka.remote.netty.tcp.port=" + localURL.getPort + ", akka.remote.netty.tcp.hostname=\"" + localURL.getHost + "\"").withFallback(ConfigFactory.load("common"))
    
    //val config = ConfigFactory.load("common.conf")
    
    this.actorSystem = ActorSystem("ChordSystem" + localURL.getPort, config)
    
    this.localNode = new NodeImpl_Actor(this.actorSystem, this.localURL)
    
    val ref : ActorRef = this.actorSystem.actorOf(Props(classOf [NodeActor], this.actorSystem, this, this.references, this.entries, this.localID, this.localURL), "nodeImpl") 
    
    this.createTasks()
  }
  
  private def createTasks(){
    this.maintenanceTasks.scheduleWithFixedDelay(new StabilizeTask(this.localNode, this.references, this.entries), this.STABILIZE_TASK_START, this.STABILIZE_TASK_INTERVAL, TimeUnit.SECONDS)
    this.maintenanceTasks.scheduleWithFixedDelay(new FixFingerTask(this.localNode, this.getID, this.references), this.FIX_FINGER_TASK_START, this.FIX_FINGER_TASK_INTERVAL, TimeUnit.SECONDS)
    this.maintenanceTasks.scheduleWithFixedDelay(new CheckPredecessorTask(this.references), this.CHECK_PREDECESSOR_TASK_START, this.CHECK_PREDECESSOR_TASK_INTERVAL, TimeUnit.SECONDS)
  }

  @throws(classOf[ServiceException])
  override def join(bootstrapURL : URL) = {
    if (bootstrapURL == null) {
      throw new NullPointerException("At least one parameter is null which is not permitted!")
    }
    if (this.localNode != null) {
      throw new ServiceException("Cannot join network; node is already connected!")
    }
    if (this.localURL == null) {
      throw new ServiceException("Node URL is not set yet! Please " + "set URL with help of setURL(URL) " + 
        "before invoking join(URL)!")
    }
    if (this.getID == null) {
      this.setID(this.hashFunction.createUniqueNodeID(this.localURL))
    }
    this.joinHelp(bootstrapURL)
  }

  @throws(classOf[ServiceException])
  override def join(localURL1 : URL, bootstrapURL : URL) = {
    if (localURL1 == null || bootstrapURL == null) {
      throw new NullPointerException("At least one parameter is null which is not permitted!")
    }
    if (this.localNode != null) {
      throw new ServiceException("Cannot join network; node is already connected!")
    }
    this.localURL = localURL1
    if (this.getID == null) {
      this.setID(this.hashFunction.createUniqueNodeID(this.localURL))
    }
    this.joinHelp(bootstrapURL)
  }

  @throws(classOf[ServiceException])
  override def join(localURL1 : URL, localID1 : ID, bootstrapURL : URL) = {
    if (localURL1 == null || localID1 == null || bootstrapURL == null) {
      throw new NullPointerException("At least one parameter is null which is not permitted!")
    }
    if (this.localNode != null) {
      throw new ServiceException("Cannot join network; node is already connected!")
    }
    this.localURL = localURL1
    this.setID(localID1)
    this.joinHelp(bootstrapURL)
  }
  
  def joinHelp(bootstrapURL : URL){
    this.entries = new Entries()
    if (NUMBER_OF_SUCCESSORS >= 1) {
      this.references = new References(this.getID, this.getURL, NUMBER_OF_SUCCESSORS, this.entries)
    } else {
      throw new RuntimeException("NUMBER_OF_SUCCESSORS intialized with wrong value! " + 
        NUMBER_OF_SUCCESSORS)
    }
    //  + ", akka.remote.netty.tcp.hostname=\"" + localURL.getHost + "\""
    val config = ConfigFactory.parseString("akka.remote.netty.tcp.port=" + localURL.getPort + ", akka.remote.netty.tcp.hostname=\"" + localURL.getHost + "\"").withFallback(ConfigFactory.load("common"))
    
    this.actorSystem = ActorSystem("ChordSystem" + localURL.getPort, config)
    
    this.localNode = new NodeImpl_Actor(this.actorSystem, this.localURL)
    
    val ref : ActorRef = this.actorSystem.actorOf(Props(classOf [NodeActor], this.actorSystem, this, this.references, this.entries, this.localID, this.localURL), "nodeImpl")
    
    var bootstrapNode: Node = null
    bootstrapNode = new NodeImpl_Actor(this.actorSystem, bootstrapURL)
    this.references.addReference(bootstrapNode)
    var mySuccessor: Node = null
    mySuccessor = bootstrapNode.findSuccessor(this.getID)
    this.logger.info(this.localURL + " has successor " + mySuccessor.getNodeURL)
    this.references.addReference(mySuccessor)
    var copyOfRefsAndEntries: RefsAndEntries = null
    copyOfRefsAndEntries = mySuccessor.notifyAndCopyEntries(this.localNode)
    var refs = copyOfRefsAndEntries.getRefs
    var predecessorSet = false
    val count = 0
    while (!predecessorSet) {
      logger.debug("Size of refs: " + refs.size)
      if (refs.size == 1) {
        logger.info("Adding successor as predecessor as there are only two peers! " + 
          mySuccessor)
        this.references.addReferenceAsPredecessor(mySuccessor)
        predecessorSet = true
        logger.debug("Actual predecessor: " + this.references.getPredecessor)
      } else {
        if (this.getID.isInInterval(refs.get(0).getNodeID, mySuccessor.getNodeID)) {
          this.references.addReferenceAsPredecessor(refs.get(0))
          predecessorSet = true
        } else {
          logger.info("Wrong successor found. Going backwards!!!")
          this.references.addReference(refs.get(0))
          copyOfRefsAndEntries = refs.get(0).notifyAndCopyEntries(this.localNode)
          refs = copyOfRefsAndEntries.getRefs
        }
      }
    }
    for (newReference <- copyOfRefsAndEntries.getRefs if newReference != null && newReference != this.localNode && 
      !this.references.containsReference(newReference)) {
      this.references.addReference(newReference)
      if (this.logger.isEnabledFor(DEBUG)) {
        this.logger.debug("Added reference on " + newReference.getNodeID + " which responded to " + "ping request")
      }
    }
    this.entries.addAll(copyOfRefsAndEntries.getEntries)
    this.createTasks()
  }

  @throws(classOf[ServiceException])
  override def leave() : Unit = {
    if (this.localNode == null && this.actorSystem == null) {
      return
    }
    this.maintenanceTasks.shutdownNow()
    try {
      val successor = this.references.getSuccessor
      if (successor != null && this.references.getPredecessor != null) {
        successor.leavesNetwork(this.references.getPredecessor)
      }
    } catch {
      case e: CommunicationException => 
    }
    this.actorSystem.shutdown()
    this.actorSystem = null
    this.asyncExecutor.shutdownNow()
    this.localNode = null
  }
  
  override def retrieve(key : Key) : Set[Serializable] = {
    if (key == null) {
      val e = new NullPointerException("Key must not have value null!")
      this.logger.error("Null pointer", e)
      throw e
    }
    val id = this.hashFunction.getHashKey(key)
    val debug = this.logger.isEnabledFor(DEBUG)
    if (debug) {
      this.logger.debug("Retrieving entries with id " + id)
    }
    var result: Set[Entry] = null
    var retrieved : Boolean = false
    while(!retrieved){
      var responsibleNode: Node = null
      responsibleNode = findSuccessor(id)
      try {
        result = responsibleNode.retrieveEntries(id)
        retrieved = true
      } catch {
        case e1: CommunicationException => {
          if (debug) {
            this.logger.debug("An error occured while invoking the retrieveEntry method " + 
              " on the appropriate node! Retrieve operation " + 
              "failed!", e1)
          }
          //continue
        }
      }
    }
    val values = new HashSet[Serializable]()
    if (result != null) {
      for (entry <- result) {
        values.add(entry.getValue)
      }
    }
    this.logger.debug("Entries were retrieved!")
    return values
  }

  override def insert(key : Key, s : Serializable) = {
    if (key == null || s == null) {
      throw new NullPointerException("Neither parameter may have value null!")
    }
    val id = this.hashFunction.getHashKey(key)
    val entryToInsert = new Entry(id, s)
    val debug = this.logger.isEnabledFor(DEBUG)
    if (debug) {
      this.logger.debug("Inserting new entry with id " + id)
    }
    var inserted = false
    while (!inserted) {
      var responsibleNode: Node = null
      responsibleNode = this.findSuccessor(id)
      if (debug) {
        this.logger.debug("Invoking insertEntry method on node " + responsibleNode.getNodeID)
      }
      try {
        responsibleNode.insertEntry(entryToInsert)
        inserted = true
      } catch {
        case e1: CommunicationException => {
          if (debug) {
            this.logger.debug("An error occured while invoking the insertEntry method " + 
              " on the appropriate node! Insert operation " + 
              "failed!", e1)
          }
          //continue
        }
      }
    }
    this.logger.debug("New entry was inserted!")
  }

  override def remove(key : Key, s : Serializable) = {
    if (key == null || s == null) {
      throw new NullPointerException("Neither parameter may have value null!")
    }
    val id = this.hashFunction.getHashKey(key)
    val entryToRemove = new Entry(id, s)
    var removed = false
    while (!removed) {
      val debug = this.logger.isEnabledFor(DEBUG)
      if (debug) {
        this.logger.debug("Removing entry with id " + id + " and value " + s)
      }
      var responsibleNode: Node = null
      responsibleNode = findSuccessor(id)
      if (debug) {
        this.logger.debug("Invoking removeEntry method on node " + responsibleNode.getNodeID)
      }
      try {
        responsibleNode.removeEntry(entryToRemove)
        removed = true
      } catch {
        case e1: CommunicationException => {
          if (debug) {
            this.logger.debug("An error occured while invoking the removeEntry method " + 
              " on the appropriate node! Remove operation " + 
              "failed!", e1)
          }
          //continue
        }
      }
    }
    this.logger.debug("Entry was removed!")
  }
  
  override def retrieve(key : Key, callback : ChordCallback) = {
    val chord = this
    this.asyncExecutor.execute(new Runnable() {

      def run() {
        var t: Throwable = null
        var result: Set[Serializable] = null
        try {
          result = chord.retrieve(key)
        } catch {
          case e: ServiceException => t = e
          case th: Throwable => t = th
        }
        callback.retrieved(key, result, t)
      }
    })
  }

  override def insert(key : Key, entry : Serializable, callback : ChordCallback) = {
    val chord = this
    this.asyncExecutor.execute(new Runnable() {

      def run() {
        var t: Throwable = null
        try {
          chord.insert(key, entry)
        } catch {
          case e: ServiceException => t = e
          case th: Throwable => t = th
        }
        callback.inserted(key, entry, t)
      }
    })
  }

  override def remove(key : Key, entry : Serializable, callback : ChordCallback) = {
    val chord = this
    this.asyncExecutor.execute(new Runnable() {

      def run() {
        var t: Throwable = null
        try {
          chord.remove(key, entry)
        } catch {
          case e: ServiceException => t = e
          case th: Throwable => t = th
        }
        callback.removed(key, entry, t)
      }
    })
  }

  override def retrieveAsync(key : Key) : ChordRetrievalFuture = {
     ChordRetrievalFutureImpl.create(this.asyncExecutor, this, key)
  }

  override def insertAsync(key : Key, entry : Serializable) : ChordFuture = {
    ChordInsertFuture.create(this.asyncExecutor, this, key, entry)
  }

  override def removeAsync(key : Key, entry : Serializable) : ChordFuture = {
    ChordRemoveFuture.create(this.asyncExecutor, this, key, entry)
  }
  
  override def printEntries() : String = this.entries.toString

  override def printFingerTable() : String = this.references.printFingerTable()

  override def printSuccessorList() : String = this.references.printSuccessorList()

  override def printReferences() : String = this.references.toString
  
  override def printPredecessor() : String = {
      val pre = this.references.getPredecessor
      if (pre == null) {
        "Predecessor: null"
      } else {
        "Predecessor: " + pre.toString
      }
  }
}