name := "Akka-ZAB"

version := "0.2"

mainClass in Compile := Some("ZAB_Application")

//scalaVersion := "2.11.7"

//scaladoc
scalacOptions in (Compile,doc) ++= Seq("-groups", "-implicits")

//compile order
compileOrder := CompileOrder.JavaThenScala

// Dependencies
// libraryDependencies += "net.liftweb" % "lift-json_2.11" % "3.0-M6"

// graph for scala
//libraryDependencies += "com.assembla.scala-incubator" % "graph-core_2.11" % "1.9.4"

//json (net.liftweb.json)
//libraryDependencies += "com.assembla.scala-incubator" % "graph-json_2.11" % "1.9.2"

// dot (graph4s to dot)
//libraryDependencies += "com.assembla.scala-incubator" % "graph-dot_2.11" % "1.10.0"

//libraryDependencies += "org.ow2.asm" % "asm-all" % "5.0.4"

//libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.5"

//libraryDependencies += "org.scalacheck" % "scalacheck_2.11" % "1.12.4"

libraryDependencies ++= Seq("com.typesafe.akka" %% "akka-actor" % "2.3.12",
                 "com.typesafe.akka" %% "akka-remote" % "2.3.12")
                            


resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
